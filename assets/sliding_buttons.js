$(function () {

   /*** SLIDING BUTTONs ***/
  var controller = new ScrollMagic.Controller();

  $(slidingButtons).each(function () {

    // slider variables
    let buttonsObject = this;
    let buttonId = buttonsObject.id;
    let sliderButtonTopPosition = buttonsObject.topButtonSection;
    let sliderButtonBottomPosition = buttonsObject.bottomButtonSection;
    let sliderButtonTopClass = buttonId+"-button-top";
    let sliderButtonBottomClass = buttonId+"-button-bottom";

    //add slider button classes
    $("section.page-section").eq(sliderButtonTopPosition-1).addClass("slider-button slider-button-top "+sliderButtonTopClass);
    $("section.page-section").eq(sliderButtonBottomPosition-1).addClass("slider-button slider-button-bottom "+sliderButtonBottomClass);

    // setup scene triggers for animation
    new ScrollMagic.Scene({
     triggerElement: "."+sliderButtonTopClass,
     triggerHook: 'onLeave' // show, when scrolled 10% into view
     })
     .setClassToggle("."+sliderButtonTopClass, "scroll-hidden")
    .reverse(false)
    .addTo(controller);
 
    new ScrollMagic.Scene({
     triggerElement: "."+sliderButtonTopClass,
     triggerHook: 'onLeave'   }).setClassToggle("."+sliderButtonBottomClass, "scroll-visible")
    .reverse(false)
     .addTo(controller);
    
    new ScrollMagic.Scene({
     triggerElement: "."+sliderButtonBottomClass,
     triggerHook: 'onEnter',
      offset: '50'})
    .setClassToggle("."+sliderButtonBottomClass, "scroll-visible")
    .reverse(true)
    .addTo(controller);
 
    new ScrollMagic.Scene({
     triggerElement: "."+sliderButtonBottomClass,
     triggerHook: 'onEnter',
      offset: '50'
    }).setClassToggle("."+sliderButtonTopClass, "scroll-hidden")
    .reverse(true)
    .addTo(controller);
    });
});