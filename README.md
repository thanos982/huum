# huum

assets for the huum.com squarespace website

## sliding buttons

Required files:
[sliding_buttons.js](assets/sliding_buttons.js)
[sliding_buttons.css](assets/sliding_buttons.css)

To enable sliding buttons on a page:
Page Settings (gear icon) > Advanced

Enter this code in the header code injection, replacing when necessary:

```
<script>
 let slidingButtons = [{
    topButtonSection: 3, // the number section of the top button
    bottomButtonSection: 9, // the section of the bottom button
    id: 'slideshow-1' // a unique id (it can be anything)
  },{
    topButtonSection: 10, // the number section of the top button
    bottomButtonSection: 12, // the section of the bottom button
    id: 'image-1' // a unique id (it can be anything)
  }];
</script>
<script src="//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/ScrollMagic.min.js"></script>
<script src="https://cdn.statically.io/gl/thanos982/huum/master/assets/sliding_buttons.js"></script>
<link href="https://cdn.statically.io/gl/thanos982/huum/master/assets/sliding_buttons.css" rel="stylesheet">
```

The slidingButtons array can have as many elements as the page requires. In the example above we have two.


## slider

The squarespace slideshows on huum.com are developed by [Will Myers](https://www.will-myers.com/products/p/sliding-image-banner)

To include them add this code in the header code injection section, replacing the values within the wmSlider object accordingly:
```
<script src="https://code.jquery.com/jquery-3.4.1.js"></script>
<script>
  let wmSlider = [{
    staticFirst: false,
    startingSection: 4,
    endingSection: 7,
    sliderTime: 5,
    autoSliderOn: false,
    playPause: false,
    sliderDots: false
  }];
</script>
<script src="https://assets.codepen.io/3198845/WMSlider6520v2-7.js"></script>
<link href="https://assets.codepen.io/3198845/WMSlider6520v2-2.css" rel="stylesheet">
```
